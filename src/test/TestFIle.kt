/*
 * kafka-streams-wikidata-stream: fetch changed wikidata entities
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.junit.Test
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset


class DatasetPipeTest {


    @Test
    fun testRequest() {
        val request = Request()
        val response = request.get("Q42")
        assert(!response.isNullOrEmpty())
    }

    @Test
    fun testFilter() {
        val filter = Filter()
        val result = filter.process("Q42",
            FileInputStream(File("/home/jonas/projects/java/kafka-streams-wikidata-stream/src/test/data.nt")).bufferedReader(
                Charset.defaultCharset()).readText())
        assert(result.isNotEmpty())
    }
}
