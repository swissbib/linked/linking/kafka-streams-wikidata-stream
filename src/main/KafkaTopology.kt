/*
 * kafka-streams-wikidata-stream: fetch changed wikidata entities
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package org.swissbib.linked

import com.beust.klaxon.Klaxon
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*


class KafkaTopology(private val properties: Properties, private val log: Logger) {
    private val request = Request()
    private val filter = Filter()

    fun build(): Topology {
        val builder = StreamsBuilder()

        builder
            .stream<String, String>(properties.getProperty("kafka.topic.source"))
            .mapValues { value -> parseJson(value) }
            .flatMapValues { value -> fetchWikidataEntity(value) }
            .mapValues { value ->  filterBySubject(value) }
            .mapValues { value -> encodeMetadata(value) }
            .to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }

    private fun parseJson(value: String): Change {
        return Klaxon().parse(value)!!
    }

    private fun fetchWikidataEntity(value: Change): List<Pair<String, String>> {
        return when (val result = request.get(value.id)) {
            is String -> listOf(Pair(value.id, result))
            else -> listOf()
        }
    }

    private fun filterBySubject(values: Pair<String, String>): String {
        return filter.process(values.first, values.second)
    }

    private fun encodeMetadata(value: String): SbMetadataModel {
        return SbMetadataModel()
            .setData(value)
            .setEsIndexName(properties.getProperty(""))
            .setEsBulkAction(EsBulkActions.INDEX)
    }
}