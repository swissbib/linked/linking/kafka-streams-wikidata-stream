/*
 * kafka-streams-wikidata-stream: fetch changed wikidata entities
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.result.Result
import org.apache.logging.log4j.LogManager
import java.nio.charset.Charset

class Request {
    private val log = LogManager.getLogger(javaClass.canonicalName)

    init {
        FuelManager.instance.basePath = "http://www.wikidata.org/wiki/Special:EntityData/"
    }

    fun get(entity: String): String? {
        val response = Fuel
            .get(entity)
            .header(Pair("Accept", "text/plain"))
            .responseString(Charset.defaultCharset())
        return when (val value = response.third) {
            is Result.Success ->
                value.get()
            is Result.Failure -> {
                log.error(value.error.message)
                null
            }
        }


    }
}