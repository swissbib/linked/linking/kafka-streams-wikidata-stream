FROM openjdk:8
ADD . /
WORKDIR /
RUN ./gradlew -q --no-scan --no-daemon --no-build-cache distTar
RUN cd /build/distributions && tar xf app.tar

FROM openjdk:8-jre-alpine
ARG jmxport=50506
ENV _JAVA_OPTIONS="\
-Dcom.sun.management.jmxremote.rmi.port=${jmxport} \
-Dcom.sun.management.jmxremote=true \
-Dcom.sun.management.jmxremote.port=${jmxport} \
-Dcom.sun.management.jmxremote.ssl=false \
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.local.only=false \
-Djava.rmi.server.hostname=0.0.0.0"
EXPOSE $jmxport
ADD src/main/resources/config.properties /app/config/config.properties
COPY --from=0 /build/distributions/app /app
CMD /app/bin/streams-app-java-skeleton
